CREATE DATABASE IF NOT EXISTS access_log;

USE access_log;

CREATE TABLE IF NOT EXISTS path (
	id INT AUTO_INCREMENT,
	name TINYTEXT NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS method (
	id INT AUTO_INCREMENT,
	name TINYTEXT NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS protocol (
	id INT AUTO_INCREMENT,
	name TINYTEXT NOT NULL,
	version TINYTEXT,
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS request (
	id INT AUTO_INCREMENT,
	path_id INT,
	method_id INT,
	protocol_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(path_id) REFERENCES path(id),
	FOREIGN KEY(method_id) REFERENCES method(id),
	FOREIGN KEY(protocol_id) REFERENCES protocol(id)
);


CREATE TABLE IF NOT EXISTS product (
	id INT AUTO_INCREMENT,
	name TINYTEXT,
	version TINYTEXT,
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS user_agent (
	id INT AUTO_INCREMENT,
	product_id INT,
	comment TEXT,
	PRIMARY KEY(id),
	FOREIGN KEY(product_id) REFERENCES product(id)
);


CREATE TABLE IF NOT EXISTS requester (
	id INT AUTO_INCREMENT,
	ip_address TINYTEXT,
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS access (
	id INT AUTO_INCREMENT,
	requester_id INT,
	request_id INT,
	user_agent_id INT,
	date_accessed TIMESTAMP(3),
	response_code SMALLINT,
	PRIMARY KEY(id),
	FOREIGN KEY(requester_id) REFERENCES requester(id),
	FOREIGN KEY(request_id) REFERENCES request(id),
	FOREIGN KEY(user_agent_id) REFERENCES user_agent(id)
);
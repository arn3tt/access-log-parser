package com.ef;

import com.ef.entities.*;
import com.ef.exceptions.IncorrectContentPatternException;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * (date|ip|request|responseCode|userAgent)
 * <p>
 * Created by arnett on 18/09/18.
 */
public class ParsingUtils {

    public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";

    public static final String VALUE_SEPARATOR_REGEX = "\\|";

    public static List<Access> parseLines(List<String> lines) throws IncorrectContentPatternException {
        List<Access> accesses = new ArrayList<Access>();
        for (String line : lines) {
            if (line.trim().length() == 0) {
                continue;
            }

            accesses.add(parseLine(line));
        }
        return accesses;
    }

    public static Access parseLine(String line) throws IncorrectContentPatternException {
        // line should be in the form date|ip|request|responseCode|userAgent

        String[] values = line.split(VALUE_SEPARATOR_REGEX);
        if (values.length != 5) {
            throw new IncorrectContentPatternException(line);
        }

        String rawAccessDate = values[0];
        String rawIp = values[1];
        String rawRequest = values[2];
        String rawResponseCode = values[3];
        String rawUserAgent = values[4];

        try {
            Date date = parseDate(rawAccessDate);
            Requester requester = parseRequester(rawIp);
            Request request = parseRequest(rawRequest);
            int responseCode = parseInteger(rawResponseCode);
            UserAgent userAgent = parseUserAgent(rawUserAgent);
            return new Access(date, requester, request, responseCode, userAgent);
        } catch (ParseException e) {
            throw new IncorrectContentPatternException(line, e);
        }
    }

    public static Date parseDate(String argument) throws ParseException {
        // example: 2017-01-01 00:01:26.856
        SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
        try {
            return format.parse(argument);
        } catch (java.text.ParseException e) {
            String message = String.format("Date must follow the pattern <%s>", DATE_PATTERN);
            throw new ParseException(message);
        }
    }

    public static String formatDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
        return format.format(date);
    }

    public static Requester parseRequester(String rawIp) throws ParseException {
        // example: 192.168.199.209
        return new Requester(rawIp);
    }

    public static UserAgent parseUserAgent(String rawUserAgent) throws ParseException {
        // example: "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Mobile Safari/537.36"
        String[] values = removeQuotes(rawUserAgent).trim().split("\\s+");

        if (values.length < 1) {
            String message = String.format("<%s> could not be parsed into a %s", rawUserAgent, UserAgent.class);
            throw new ParseException(message);
        }

        Product product = parseProduct(values[0]);
        String comment = getComment(values);
        return new UserAgent(product, comment);
    }

    private static Product parseProduct(String rawProduct) throws ParseException {
        // example: Mozilla/5.0
        String[] values = rawProduct.split("/");

        String name;
        String version;
        if (values.length == 1) {
            name = values[0];
            version = null;
        } else if (values.length == 2) {
            name = values[0];
            version = values[1];
        } else {
            String message = String.format("<%s> could not be parsed into a %s", rawProduct, Product.class);
            throw new ParseException(message);
        }

        return new Product(name, version);
    }

    private static String getComment(String[] values) {
        // removing the first element
        String[] copiedValues = new String[values.length - 1];
        for (int i = 1; i < values.length; i++) {
            copiedValues[i - 1] = values[i];
        }

        return StringUtils.join(copiedValues, " ");
    }

    public static Request parseRequest(String rawRequest) throws ParseException {
        // example: "GET / HTTP/1.1"
        String[] values = removeQuotes(rawRequest).trim().split("\\s+");

        if (values.length != 3) {
            String message = String.format("<%s> could not be parsed into a %s", rawRequest, Request.class);
            throw new ParseException(message);
        }

        String methodName = values[0];
        String pathName = values[1];
        String rawProtocol = values[2];
        Method method = new Method(methodName);
        Protocol protocol = parseProtocol(rawProtocol);
        Path path = new Path(pathName);
        return new Request(method, protocol, path);
    }

    private static Protocol parseProtocol(String rawProtocol) throws ParseException {
        // example: HTTP/1.1
        String[] values = rawProtocol.split("/");

        if (values.length != 2) {
            String message = String.format("<%s> could not be parsed into a %s", rawProtocol, Protocol.class);
            throw new ParseException(message);
        }

        String name = values[0];
        String version = values[1];
        return new Protocol(name, version);
    }

    public static int parseInteger(String rawInteger) throws ParseException {
        try {
            return Integer.parseInt(rawInteger);
        } catch (NumberFormatException e) {
            throw new ParseException(String.format("%s is not a number", rawInteger));
        }
    }

    private static String removeQuotes(String string) {
        return string.replaceAll("\"", "");
    }

}

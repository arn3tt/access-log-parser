package com.ef.database;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnector {

    private static final String DATABASE_CONF_FILE_NAME = "database.conf";

    private static final String SERVER_KEY = "server";
    private static final String DATABASE_NAME_KEY = "database_name";
    private static final String USERNAME_KEY = "username";
    private static final String PASSWORD_KEY = "password";

    public static java.sql.Connection connect() throws IOException {
        // TODO create conf file to hold database credentials
        try {
            String driverName = "com.mysql.jdbc.Driver";
            Class.forName(driverName);

            Properties databaseProperties = new Properties();
            databaseProperties.load(new FileReader(DATABASE_CONF_FILE_NAME));
            String serverName = databaseProperties.get(SERVER_KEY).toString();
            String databaseName = databaseProperties.get(DATABASE_NAME_KEY).toString();
            String url = String.format("jdbc:mysql://%s/%s", serverName, databaseName);

            String username = databaseProperties.get(USERNAME_KEY).toString();
            String password = databaseProperties.get(PASSWORD_KEY).toString();
            return DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
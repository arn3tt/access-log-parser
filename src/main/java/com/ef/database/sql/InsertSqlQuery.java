package com.ef.database.sql;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by arnett on 19/09/18.
 */
public abstract class InsertSqlQuery implements SqlQuery {

    public static final String INSERT_QUERY_FORMAT =
            "INSERT INTO %s (%s) VALUES (%s)";

    public abstract String getTableName();

    protected abstract String[] getColumns();

    protected abstract String[] getValues();

    public String toQuery() {
        String columns = StringUtils.join(getColumns(), ", ");
        String values = StringUtils.join(getValues(), ", ");
        return String.format(INSERT_QUERY_FORMAT, getTableName(), columns, values);
    }

}

package com.ef.database.sql;

/**
 * Created by arnett on 19/09/18.
 */
public class Constants {

    public static class Access {

        public static final String TABLE_NAME = "access";
        public static final String[] COLUMNS = new String[]{
                "requester_id", "request_id", "user_agent_id", "date_accessed", "response_code"
        };

    }

    public static class Requester {

        public static final String TABLE_NAME = "requester";
        public static final String[] COLUMNS = new String[]{
                "ip_address"
        };
    }

    public static class UserAgent {

        public static final String TABLE_NAME = "user_agent";
        public static final String[] COLUMNS = new String[]{
                "product_id", "comment"
        };
    }

    public static class Request {

        public static final String TABLE_NAME = "request";
        public static final String[] COLUMNS = new String[]{
                "path_id", "method_id", "protocol_id"
        };

    }

    public static class Path {

        public static final String TABLE_NAME = "path";
        public static final String[] COLUMNS = new String[]{
                "name"
        };

    }

    public static class Method {

        public static final String TABLE_NAME = "method";
        public static final String[] COLUMNS = new String[]{
                "name"
        };

    }

    public static class Protocol {

        public static final String TABLE_NAME = "protocol";
        public static final String[] COLUMNS = new String[]{
                "name", "version"
        };

    }

    public static class Product {

        public static final String TABLE_NAME = "product";
        public static final String[] COLUMNS = new String[]{
                "name", "version"
        };

    }

    public static class Access5 {

        public static final String TABLE_NAME = "a123123ccess";

    }

}

package com.ef.database.sql.access;

import com.ef.database.sql.Constants;
import com.ef.database.sql.InsertSqlQuery;

/**
 * Created by arnett on 19/09/18.
 */
public class InsertRequesterSqlQuery extends InsertSqlQuery {

    private String ipAddress;

    public InsertRequesterSqlQuery(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getTableName() {
        return Constants.Requester.TABLE_NAME;
    }

    protected String[] getColumns() {
        return Constants.Requester.COLUMNS;
    }

    protected String[] getValues() {
        return new String[]{"'" + ipAddress + "'"};
    }

}

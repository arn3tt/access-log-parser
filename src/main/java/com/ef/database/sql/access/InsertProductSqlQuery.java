package com.ef.database.sql.access;

import com.ef.database.sql.Constants;
import com.ef.database.sql.InsertSqlQuery;

/**
 * Created by arnett on 19/09/18.
 */
public class InsertProductSqlQuery extends InsertSqlQuery {

    private String name;
    private String version;

    public InsertProductSqlQuery(String name, String version) {
        this.name = name;
        this.version = version;
    }

    public String getTableName() {
        return Constants.Product.TABLE_NAME;
    }

    protected String[] getColumns() {
        return Constants.Product.COLUMNS;
    }

    protected String[] getValues() {
        return new String[]{
                "'" + name + "'",
                "'" + version + "'"
        };
    }

}

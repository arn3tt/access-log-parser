package com.ef.database.sql.access;

import com.ef.database.sql.Constants;
import com.ef.database.sql.InsertSqlQuery;

/**
 * Created by arnett on 19/09/18.
 */
public class InsertPathSqlQuery extends InsertSqlQuery {

    private String name;

    public InsertPathSqlQuery(String name) {
        this.name = name;
    }

    public String getTableName() {
        return Constants.Path.TABLE_NAME;
    }

    protected String[] getColumns() {
        return Constants.Path.COLUMNS;
    }

    protected String[] getValues() {
        return new String[]{
                "'" + name + "'"
        };
    }

}

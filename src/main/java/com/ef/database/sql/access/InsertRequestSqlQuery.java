package com.ef.database.sql.access;

import com.ef.database.sql.Constants;
import com.ef.database.sql.InsertSqlQuery;

/**
 * Created by arnett on 19/09/18.
 */
public class InsertRequestSqlQuery extends InsertSqlQuery {

    private int pathId;
    private int methodId;
    private int protocolId;

    public InsertRequestSqlQuery(int pathId, int methodId, int protocolId) {
        this.pathId = pathId;
        this.methodId = methodId;
        this.protocolId = protocolId;
    }

    public String getTableName() {
        return Constants.Request.TABLE_NAME;
    }

    protected String[] getColumns() {
        return Constants.Request.COLUMNS;
    }

    protected String[] getValues() {
        return new String[]{
                String.valueOf(pathId),
                String.valueOf(methodId),
                String.valueOf(protocolId)
        };
    }

}

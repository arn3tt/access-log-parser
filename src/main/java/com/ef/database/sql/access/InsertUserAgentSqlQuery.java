package com.ef.database.sql.access;

import com.ef.database.sql.Constants;
import com.ef.database.sql.InsertSqlQuery;

/**
 * Created by arnett on 19/09/18.
 */
public class InsertUserAgentSqlQuery extends InsertSqlQuery {

    private int productId;
    private String comment;

    public InsertUserAgentSqlQuery(int productId, String comment) {
        this.productId = productId;
        this.comment = comment;
    }

    public String getTableName() {
        return Constants.UserAgent.TABLE_NAME;
    }

    protected String[] getColumns() {
        return Constants.UserAgent.COLUMNS;
    }

    protected String[] getValues() {
        return new String[]{
                "'" + String.valueOf(productId) + "'",
                "'" + comment + "'"
        };
    }

}

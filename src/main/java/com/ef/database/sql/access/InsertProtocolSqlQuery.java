package com.ef.database.sql.access;

import com.ef.database.sql.Constants;
import com.ef.database.sql.InsertSqlQuery;

/**
 * Created by arnett on 19/09/18.
 */
public class InsertProtocolSqlQuery extends InsertSqlQuery {

    private String name;
    private String version;

    public InsertProtocolSqlQuery(String name, String version) {
        this.name = name;
        this.version = version;
    }

    public String getTableName() {
        return Constants.Protocol.TABLE_NAME;
    }

    protected String[] getColumns() {
        return Constants.Protocol.COLUMNS;
    }

    protected String[] getValues() {
        return new String[]{
                "'" + name + "'",
                "'" + version + "'"
        };
    }

}

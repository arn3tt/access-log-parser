package com.ef.database.sql.access;

import com.ef.database.sql.Constants;
import com.ef.database.sql.InsertSqlQuery;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.ef.ParsingUtils.DATE_PATTERN;

/**
 * Created by arnett on 19/09/18.
 */
public class InsertAccessSqlQuery extends InsertSqlQuery {

    private int requesterId;
    private int requestId;
    private int userAgentId;
    private Date dateAccessed;
    private int responseCode;

    public InsertAccessSqlQuery(int requesterId, int requestId, int userAgentId, Date dateAccessed, int responseCode) {
        this.requesterId = requesterId;
        this.requestId = requestId;
        this.userAgentId = userAgentId;
        this.dateAccessed = dateAccessed;
        this.responseCode = responseCode;
    }

    public String getTableName() {
        return Constants.Access.TABLE_NAME;
    }

    protected String[] getColumns() {
        return Constants.Access.COLUMNS;
    }

    protected String[] getValues() {
        return new String[]{
                String.valueOf(requesterId),
                String.valueOf(requestId),
                String.valueOf(userAgentId),
                "'" + new SimpleDateFormat(DATE_PATTERN).format(dateAccessed) + "'",
                String.valueOf(responseCode),
        };
    }

}

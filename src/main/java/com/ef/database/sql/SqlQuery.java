package com.ef.database.sql;

/**
 * Created by arnett on 19/09/18.
 */
public interface SqlQuery {

    String toQuery();

}

package com.ef.database;

import com.ef.database.exceptions.ResourceNotFoundException;
import com.ef.database.sql.Constants;
import com.ef.database.sql.access.*;
import com.ef.entities.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by arnett on 19/09/18.
 */
public class DatabaseManager {

    private final Connection connection;

    private Map<CachedEntity, Integer> cachedEntities = new HashMap<CachedEntity, Integer>();

    public DatabaseManager(Connection connection) {
        this.connection = connection;
    }

    public void insert(Access access) throws SQLException, ResourceNotFoundException {
        int requesterId;
        try {
            requesterId = getId(access.getRequester());
        } catch (ResourceNotFoundException e) {
            insert(access.getRequester());
            requesterId = getId(access.getRequester());
        }

        int userAgentId = insert(access.getUserAgent());

        int requestId = insert(access.getRequest());

        InsertAccessSqlQuery query = new InsertAccessSqlQuery(
                requesterId, requestId, userAgentId,
                access.getDateAccessed(), access.getResponseCode());
        executeUpdateQuery(query.toQuery());
    }

    public int insert(Request request) throws ResourceNotFoundException, SQLException {
        int pathId;
        try {
            pathId = getId(request.getPath());
        } catch (ResourceNotFoundException e) {
            insert(request.getPath());
            pathId = getId(request.getPath());
        }

        int methodId;
        try {
            methodId = getId(request.getMethod());
        } catch (ResourceNotFoundException e) {
            insert(request.getMethod());
            methodId = getId(request.getMethod());
        }

        int protocolId;
        try {
            protocolId = getId(request.getProtocol());
        } catch (ResourceNotFoundException e) {
            insert(request.getProtocol());
            protocolId = getId(request.getProtocol());
        }

        InsertRequestSqlQuery query = new InsertRequestSqlQuery(pathId, methodId, protocolId);
        executeUpdateQuery(query.toQuery());

        String whereClause = String.format(
                "path_id = '%s' AND method_id = '%s' AND protocol_id = '%s'",
                pathId,
                methodId,
                protocolId);

        return getId(Constants.Request.TABLE_NAME, whereClause);
    }

    public int insert(UserAgent userAgent) throws ResourceNotFoundException, SQLException {
        int productId;
        try {
            productId = getId(userAgent.getProduct());
        } catch (ResourceNotFoundException e) {
            insert(userAgent.getProduct());
            productId = getId(userAgent.getProduct());
        }

        String comment = userAgent.getComment();
        InsertUserAgentSqlQuery query = new InsertUserAgentSqlQuery(productId, comment);
        executeUpdateQuery(query.toQuery());

        String whereClause = String.format(
                "product_id = '%s'",
                productId);

        return getId(Constants.UserAgent.TABLE_NAME, whereClause);
    }

    public void insert(Requester requester) throws SQLException {
        InsertRequesterSqlQuery query = new InsertRequesterSqlQuery(requester.getIpAddress());
        executeUpdateQuery(query.toQuery());
    }

    private void insert(Protocol protocol) throws SQLException {
        InsertProtocolSqlQuery query = new InsertProtocolSqlQuery(protocol.getName(), protocol.getVersion());
        executeUpdateQuery(query.toQuery());
    }

    private void insert(Method method) throws SQLException {
        InsertMethodSqlQuery query = new InsertMethodSqlQuery(method.getName());
        executeUpdateQuery(query.toQuery());
    }

    private void insert(Path path) throws SQLException {
        InsertPathSqlQuery query = new InsertPathSqlQuery(path.getName());
        executeUpdateQuery(query.toQuery());
    }

    private void insert(Product product) throws SQLException {
        InsertProductSqlQuery query = new InsertProductSqlQuery(product.getName(), product.getVersion());
        executeUpdateQuery(query.toQuery());
    }

    public int getId(Requester requester) throws ResourceNotFoundException, SQLException {
        Integer id = cachedEntities.get(requester);
        if (id == null) {
            String whereClause = String.format(
                    "ip_address = '%s'",
                    requester.getIpAddress());

            id = getId(Constants.Requester.TABLE_NAME, whereClause);
        }

        return id;
    }

    private int getId(Protocol protocol) throws ResourceNotFoundException, SQLException {
        Integer id = cachedEntities.get(protocol);
        if (id == null) {
            String whereClause = String.format(
                    "name = '%s' AND version = '%s'",
                    protocol.getName(),
                    protocol.getVersion());

            id = getId(Constants.Protocol.TABLE_NAME, whereClause);
        }

        return id;
    }

    private int getId(Method method) throws ResourceNotFoundException, SQLException {
        Integer id = cachedEntities.get(method);
        if (id == null) {
            String whereClause = String.format(
                    "name = '%s'",
                    method.getName());

            id = getId(Constants.Method.TABLE_NAME, whereClause);
        }

        return id;
    }

    private int getId(Path path) throws ResourceNotFoundException, SQLException {
        Integer id = cachedEntities.get(path);
        if (id == null) {
            String whereClause = String.format(
                    "name = '%s'",
                    path.getName());

            id = getId(Constants.Path.TABLE_NAME, whereClause);
        }

        return id;
    }

    private int getId(Product product) throws ResourceNotFoundException, SQLException {
        Integer id = cachedEntities.get(product);
        if (id == null) {
            String whereClause = String.format(
                    "name = '%s' AND version = '%s'",
                    product.getName(),
                    product.getVersion());

            id = getId(Constants.Product.TABLE_NAME, whereClause);
        }

        return id;
    }

    private int getId(String tableName, String whereClause) throws ResourceNotFoundException, SQLException {
        String query;
        if (whereClause == null && whereClause.length() == 0) {
            query = String.format("SELECT id FROM %s", tableName);
        } else {
            query = String.format("SELECT id FROM %s WHERE %s", tableName, whereClause);
        }

        return executeGetIdQuery(query);
    }

    public int executeGetIdQuery(String query) throws SQLException, ResourceNotFoundException {
        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery(query);
        if (resultSet.next()) {
            try {
                return resultSet.getInt("id");
            } catch (SQLException e) {
            }
        }

        throw new ResourceNotFoundException();
    }

    public void executeUpdateQuery(String query) throws SQLException {
        Statement statement = connection.createStatement();
        int i = statement.executeUpdate(query);
    }

    public static void main(String[] args) throws SQLException, ResourceNotFoundException {
//        Product product = new Product("productName", "productVersion");
//        insert(product);
//        int id = getId(product);
//        System.out.println(id);

//        Connection connection = DatabaseConnector.connect();
//
//        Statement statement = connection.createStatement();
//        statement.executeUpdate(String.format("INSERT INTO requester (ip_address) VALUES (\"%s\")", UUID.randomUUID().toString()));
//
//        statement = connection.createStatement();
//        ResultSet resultSet = statement.executeGetIdQuery("SELECT * FROM requester");
//        connection.close();
    }

}

package com.ef.entities;

/**
 * Created by arnett on 18/09/18.
 */
public class Path extends CachedEntity {

    private String name;

    public Path(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Path path = (Path) o;

        return name != null ? name.equals(path.name) : path.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}

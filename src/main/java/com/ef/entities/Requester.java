package com.ef.entities;

/**
 * Created by arnett on 18/09/18.
 */
public class Requester extends CachedEntity {

    private String ipAddress;

    public Requester(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Requester requester = (Requester) o;

        return ipAddress != null ? ipAddress.equals(requester.ipAddress) : requester.ipAddress == null;
    }

    @Override
    public int hashCode() {
        return ipAddress != null ? ipAddress.hashCode() : 0;
    }

}

package com.ef.entities;

/**
 * Created by arnett on 18/09/18.
 */
public class Request {

    private Method method;
    private Protocol protocol;
    private Path path;

    public Request(Method method, Protocol protocol, Path path) {
        this.method = method;
        this.protocol = protocol;
        this.path = path;
    }

    public Method getMethod() {
        return method;
    }

    public Protocol getProtocol() {
        return protocol;
    }

    public Path getPath() {
        return path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Request request = (Request) o;

        if (method != null ? !method.equals(request.method) : request.method != null) return false;
        if (protocol != null ? !protocol.equals(request.protocol) : request.protocol != null) return false;
        return path != null ? path.equals(request.path) : request.path == null;
    }

}

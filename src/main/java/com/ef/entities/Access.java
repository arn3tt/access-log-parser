package com.ef.entities;

import java.util.Date;

/**
 * Created by arnett on 12/09/18.
 */
public class Access {

    /**
     * SQL Statements
     * <p>
     * CREATE TABLE accesses( accessed TIMESTAMP(3),
     */


    private Date dateAccessed;
    private Requester requester;
    private Request request;
    private int responseCode;
    private UserAgent userAgent;

    public Access(Date dateAccessed, Requester requester, Request request, int responseCode, UserAgent userAgent) {
        this.dateAccessed = dateAccessed;
        this.requester = requester;
        this.request = request;
        this.responseCode = responseCode;
        this.userAgent = userAgent;
    }

    public Requester getRequester() {
        return requester;
    }

    public Request getRequest() {
        return request;
    }

    public UserAgent getUserAgent() {
        return userAgent;
    }

    public Date getDateAccessed() {
        return dateAccessed;
    }

    public int getResponseCode() {
        return responseCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Access access = (Access) o;

        if (responseCode != access.responseCode) return false;
        if (requester != null ? !requester.equals(access.requester) : access.requester != null) return false;
        if (request != null ? !request.equals(access.request) : access.request != null) return false;
        if (userAgent != null ? !userAgent.equals(access.userAgent) : access.userAgent != null) return false;
        return dateAccessed != null ? dateAccessed.equals(access.dateAccessed) : access.dateAccessed == null;
    }

}

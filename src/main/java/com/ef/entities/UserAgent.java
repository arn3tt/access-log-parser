package com.ef.entities;

/**
 * Created by arnett on 12/09/18.
 */
public class UserAgent {

    private Product product;
    private String comment;

    public UserAgent(Product product, String comment) {
        this.product = product;
        this.comment = comment;
    }

    public Product getProduct() {
        return product;
    }

    public String getComment() {
        return comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAgent userAgent = (UserAgent) o;

        if (product != null ? !product.equals(userAgent.product) : userAgent.product != null) return false;
        boolean b = comment != null ? comment.equals(userAgent.comment) : userAgent.comment == null;
        return b;
    }

}

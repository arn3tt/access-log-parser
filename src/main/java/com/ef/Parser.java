package com.ef;

import org.apache.commons.cli.*;

import java.util.Date;

/**
 * Created by arnett on 12/09/18.
 */
public class Parser {

    public static final int SECOND = 1;
    public static final int MINUTE = 60 * SECOND;
    public static final long HOUR = 60 * MINUTE;
    public static final long DAY = 24 * HOUR;

    public static final String START_DATE_OPTION = "startDate";
    public static final String DURATION_OPTION = "duration";
    public static final String THRESHOLD_OPTION = "threshold";

    public static final String START_DATE_ARG_NAME = "date";
    public static final String DURATION_ARG_NAME = "interval";
    public static final String THRESHOLD_ARG_NAME = "limit";

    public static final String DURATION_DESCRIPTION = "the duration (either 'hourly' or 'daily')";
    public static final String START_DATE_DESCRIPTION = "the initial date";
    public static final String THRESHOLD_DESCRIPTION = "a number which is limit of requests in the provided interval";

    public static void main(String[] args) {
        Options options = createParserOptions();
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse(options, args);
            Date startDate = ParsingUtils.parseDate(line.getOptionValue(START_DATE_OPTION));
            long intervalInSeconds = intervalFromDuration(line.getOptionValue(DURATION_OPTION));
            int threshold = ParsingUtils.parseInteger(line.getOptionValue(THRESHOLD_OPTION));
            return;
        } catch (ParseException e) {
        }

        // if the main method gets here, an error occurred
        printUsage(options);
    }

    /**
     * Returns the amount of seconds in the specified duration.
     *
     * @param duration must be either 'hourly' or 'daily'
     * @return the amount of seconds in the specified duration
     */
    protected static long intervalFromDuration(String duration) throws ParseException {
        if (duration.equalsIgnoreCase("daily")) {
            return DAY;
        } else if (duration.equalsIgnoreCase("hourly")) {
            return HOUR;
        } else {
            throw new ParseException("duration must be either 'hourly' or 'daily'");
        }
    }

    protected static CommandLine parseArgs(String[] args) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        return parser.parse(createParserOptions(), args);
    }

    private static void printUsage(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("com.ef.Parser", options);
    }

    private static Options createParserOptions() {
        Option startDate = Option.builder()
                .longOpt(START_DATE_OPTION)
                .argName(START_DATE_ARG_NAME)
                .desc(START_DATE_DESCRIPTION)
                .hasArg()
                .required()
                .build();

        Option duration = Option.builder()
                .longOpt(DURATION_OPTION)
                .argName(DURATION_ARG_NAME)
                .desc(DURATION_DESCRIPTION)
                .hasArg()
                .required()
                .build();

        Option threshold = Option.builder()
                .longOpt(THRESHOLD_OPTION)
                .argName(THRESHOLD_ARG_NAME)
                .desc(THRESHOLD_DESCRIPTION)
                .hasArg()
                .required()
                .build();

        Options options = new Options();
        options.addOption(startDate);
        options.addOption(duration);
        options.addOption(threshold);
        return options;
    }

}

package com.ef;

import com.ef.database.DatabaseConnector;
import com.ef.database.DatabaseManager;
import com.ef.database.exceptions.ResourceNotFoundException;
import com.ef.entities.Access;
import com.ef.exceptions.IncorrectContentPatternException;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by arnett on 18/09/18.
 */
public class Loader {

    public static final String MALFORMED_FILE_ERROR_MESSAGE = "All lines in the file must follow the pattern date|ip|request|responseCode|userAgent";

    public static final String INPUT_FILE_OPTION = "inputFile";
    public static final String INPUT_FILE_ARG_NAME = "file";
    public static final String INPUT_FILE_DESCRIPTION = "the access log file";

    public static void main(String[] args) throws ResourceNotFoundException, SQLException {
        Options options = createLoaderOptions();
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse(options, args);
            String fileName = line.getOptionValue(INPUT_FILE_OPTION);

            File file = new File(fileName);
            if (file.exists()) {
                List<String> lines = FileUtils.readLines(file);
                List<Access> accesses = ParsingUtils.parseLines(lines);
                loadIntoDatabase(accesses);
                return;
            }
        } catch (IncorrectContentPatternException e) {
            System.err.println(MALFORMED_FILE_ERROR_MESSAGE);
            return;
        } catch (ParseException e) {
        } catch (IOException e) {
        }

        // if the main method gets here, an error occurred
        printUsage(options);
    }

    private static void loadIntoDatabase(List<Access> accesses) throws ResourceNotFoundException, SQLException, IOException {
        Connection connection = DatabaseConnector.connect();
        try {
            DatabaseManager databaseManager = new DatabaseManager(connection);
            for (Access access : accesses) {
                databaseManager.insert(access);
            }
        } finally {
            connection.close();
        }
    }

    private static void printUsage(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("com.ef.Loader", options);
    }

    private static Options createLoaderOptions() {
        Option inputFile = Option.builder()
                .longOpt(INPUT_FILE_OPTION)
                .argName(INPUT_FILE_ARG_NAME)
                .desc(INPUT_FILE_DESCRIPTION)
                .hasArg()
                .required()
                .build();

        Options options = new Options();
        options.addOption(inputFile);
        return options;
    }
}

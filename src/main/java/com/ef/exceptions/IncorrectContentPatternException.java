package com.ef.exceptions;

/**
 * Created by arnett on 18/09/18.
 */
public class IncorrectContentPatternException extends Exception {

    public IncorrectContentPatternException(String message) {
        super(message);
    }

    public IncorrectContentPatternException(String message, Throwable cause) {
        super(message, cause);
    }
}

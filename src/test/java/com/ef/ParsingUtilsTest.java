package com.ef;

import com.ef.entities.*;
import com.ef.exceptions.IncorrectContentPatternException;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by arnett on 18/09/18.
 */
public class ParsingUtilsTest {

    @Test
    public void testParseDate() throws ParseException {
        String sampleDate = "2017-01-01 17:43:09.106";
        Date date = ParsingUtils.parseDate(sampleDate);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        assertEquals(2017, calendar.get(Calendar.YEAR));
        assertEquals(0, calendar.get(Calendar.MONTH)); // 0 is january
        assertEquals(1, calendar.get(Calendar.DAY_OF_MONTH));
        assertEquals(17, calendar.get(Calendar.HOUR_OF_DAY));
        assertEquals(43, calendar.get(Calendar.MINUTE));
        assertEquals(9, calendar.get(Calendar.SECOND));
        assertEquals(106, calendar.get(Calendar.MILLISECOND));
    }

    @Test
    public void testParseLine() throws IncorrectContentPatternException {
        // TODO Extract values to variables for reuse and remove magic numbers
        String line = "2017-01-01 00:01:26.918|192.168.38.236|\"GET / HTTP/1.1\"|200|\"swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0\"";
        Access access = ParsingUtils.parseLine(line);

        Calendar cal = Calendar.getInstance();
        cal.set(2017, Calendar.JANUARY, 1, 0, 1, 26);
        cal.set(Calendar.MILLISECOND, 918);
        Date expectedDate = cal.getTime();
        assertEquals(expectedDate, access.getDateAccessed());

        Requester expectedRequester = new Requester("192.168.38.236");
        assertEquals(expectedRequester, access.getRequester());

        Method expectedMethod = new Method("GET");
        Path expectedPath = new Path("/");
        Protocol expectedProtocol = new Protocol("HTTP", "1.1");
        Request expectedRequest = new Request(expectedMethod, expectedProtocol, expectedPath);
        assertEquals(expectedRequest, access.getRequest());

        Product expectedProduct = new Product("swcd", null);
        UserAgent expectedUserAgent = new UserAgent(expectedProduct, "(unknown version) CFNetwork/808.2.16 Darwin/15.6.0");
        assertEquals(expectedUserAgent, access.getUserAgent());
        assertEquals(200, access.getResponseCode());
    }

    @Test
    public void testParseMultipleLines() throws IOException, IncorrectContentPatternException {
        File accessLogFile = new File("src/main/resources/access.log");
        List<String> lines = FileUtils.readLines(accessLogFile);
        List<Access> accesses = ParsingUtils.parseLines(lines);
        assertEquals(116484, accesses.size());
    }


}

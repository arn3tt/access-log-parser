package com.ef;

import com.ef.database.exceptions.ResourceNotFoundException;
import org.junit.Ignore;
import org.junit.Test;

import java.sql.SQLException;

/**
 * Created by arnett on 19/09/18.
 */
public class LoaderTest {

    @Test
    @Ignore
    public void testInsertingOneAccessRecord() throws ResourceNotFoundException, SQLException {
        Loader.main(new String[] { "--inputFile", "src/main/resources/small_access.log"});
    }

    @Test
    public void testInsertingExampleFile() throws ResourceNotFoundException, SQLException {
        Loader.main(new String[] { "--inputFile", "src/main/resources/access.log"});
    }
}

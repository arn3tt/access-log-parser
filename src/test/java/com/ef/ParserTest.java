package com.ef;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by arnett on 18/09/18.
 */
public class ParserTest {

    @Test
    public void testParseArgs() throws ParseException {
        String exampleDate = "2017-01-01.13:00:00";
        String exampleDuration = "daily";
        String exampleThreshold = "250";

        String[] args = new String[]{
                String.format("--%s=%s", Parser.START_DATE_OPTION, exampleDate),
                String.format("--%s=%s", Parser.DURATION_OPTION, exampleDuration),
                String.format("--%s=%s", Parser.THRESHOLD_OPTION, exampleThreshold),
        };

        CommandLine line = Parser.parseArgs(args);

        assertTrue(line.hasOption(Parser.START_DATE_OPTION));
        assertTrue(line.hasOption(Parser.DURATION_OPTION));
        assertTrue(line.hasOption(Parser.THRESHOLD_OPTION));

        assertEquals(exampleDate, line.getOptionValue(Parser.START_DATE_OPTION));
        assertEquals(exampleDuration, line.getOptionValue(Parser.DURATION_OPTION));
        assertEquals(exampleThreshold, line.getOptionValue(Parser.THRESHOLD_OPTION));
    }

    @Test
    public void testIntervalFromDuration() throws ParseException {
        int second = 1;
        int minute = 60 * second;
        long hour = 60 * minute;
        long day = 24 * hour;

        assertEquals(day, Parser.intervalFromDuration("daily"));
        assertEquals(hour, Parser.intervalFromDuration("hourly"));
    }

}

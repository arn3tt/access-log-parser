package com.ef.database.sql.access;


import com.ef.database.sql.Constants;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by arnett on 19/09/18.
 */
public class InsertAccessSqlQueryTest {

    /*
    CREATE TABLE IF NOT EXISTS test_temp(
        t timestamp(3)
    );

    INSERT INTO test_temp (t) VALUES ('2017-01-01 23:59:51.255');

    SELECT * FROM test_temp;
     */

    @Test
    public void testToQuery() {
        Calendar cal = Calendar.getInstance();
        cal.set(2017, Calendar.JANUARY, 1, 0, 1, 26);
        cal.set(Calendar.MILLISECOND, 918);
        Date date = cal.getTime();

        String EXPECTED_QUERY = "INSERT INTO %s (%s) VALUES (0, 0, 0, '2017-01-01 00:01:26.918', 0)";
        EXPECTED_QUERY = String.format(EXPECTED_QUERY,
                Constants.Access.TABLE_NAME,
                StringUtils.join(Constants.Access.COLUMNS, ", "));

        InsertAccessSqlQuery query = new InsertAccessSqlQuery(0, 0, 0, date, 0);
        assertEquals(EXPECTED_QUERY, query.toQuery());
    }

}
